import styled from 'styled-components'

import { theme } from '@styles/themes'

export const Load = styled.h1`
  color: ${theme.colors.white};
  background-color: ${theme.colors.grey};
  padding: ${theme.RANGE.L}px;
  border-radius: ${theme.BORDER_RADIUS.L}px;
`
